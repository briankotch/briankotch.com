import { IProfile, bio } from "../models/bio";

/*
Typescript dynamic typing of my resume / CV 
Loads profile.json and exposes the types to typescript
*/
class BioController {
    _bio: IProfile = bio;

    get bio() {
        return this._bio;
    }

    get jobs() {
        return this._bio.jobs;
    }

    filterJobs(tags: string[]) {
        tags = tags.sort();
        return this._bio.jobs.filter((job) => {
            return tags.some((tag) => {
                return job.tags.indexOf(tag) >= 0;
            });
        });
    }
}

const bioController: BioController = new BioController();

export default bioController;
