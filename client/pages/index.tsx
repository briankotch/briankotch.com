import React from "react";
import Divider from "./components/Divider";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Nav from "./components/Nav";
import Propaganda from "./components/Propaganda";
import styles from "./index.module.scss";

class Home extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <div className="main">
                    <div className={`container`}>
                        <main className={`content ${styles.mtfuji}`}>
                            <section className={`ink-strokes`}>
                                <h1>Welcome!</h1>
                                <p>
                                    This is my professional web site designed to
                                    show off my skills and provide me a sandbox
                                    for experimentation.
                                </p>
                            </section>
                            <Divider />
                            <section>
                                <h2>About this site</h2>
                                <hr />
                                <p>
                                    This site is the culmination of my
                                    full-stack skills. It is completely
                                    overbuilt, but I wanted to have a central
                                    place to show that:
                                </p>
                                <ol>
                                    <li>
                                        <span>
                                            I can build a front-end in a modern
                                            framework like React / NextJS.
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            I can a build a microserve backend
                                            in Node and Python.
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            I can deploy an entire stack from
                                            Devops / Platform-As-Code /
                                            Infrastructure-As-Code mindset.
                                        </span>
                                    </li>
                                    <li>
                                        <span>
                                            I can demonstrate the various skills
                                            and challenges I have been asked to
                                            do as programming tests over the
                                            years.
                                        </span>
                                    </li>
                                </ol>
                                <p>
                                    The client is only the tip of the iceberg.
                                    It has some serious depth. It is:
                                </p>
                                <ol>
                                    <li>
                                        <span>server-side-rendered</span>
                                    </li>
                                    <li>
                                        <span>
                                            strongly typed with Typescript
                                        </span>
                                    </li>
                                    <li>
                                        <span>lint-free and prettified</span>
                                    </li>
                                    <li>
                                        <span>modular / DRY architecture</span>
                                    </li>
                                    <li>
                                        <span>
                                            Semantic HTML5 and modular sass
                                        </span>
                                    </li>
                                </ol>
                            </section>
                            <Divider />
                            <section>
                                <h2>It is dynamic</h2>
                                <hr />
                                <p>
                                    Presigned JWT tokens issued to the client
                                    and server secure api calls.
                                </p>
                                <p>
                                    NextJSs api forwards calls to RESTful
                                    backends.
                                </p>
                            </section>
                        </main>
                        <Nav />
                        <Propaganda />
                    </div>
                    <Footer />
                </div>
            </div>
        );
    }
}

export default Home;
