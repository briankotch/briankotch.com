import React from "react";
import Markdown from "react-markdown";
import styles from "./IsPrime.module.scss";
import { IIsPrimeResponse } from "../../api/is_prime";

interface IIsPrimeProps {
    content: string;
}

interface IIsPrimeState {
    algorithm: "atkin" | "eratosthenes";
    number: string;
    result?: string;
}

class IsPrimeForm extends React.Component<IIsPrimeProps, IIsPrimeState> {
    constructor(props: IIsPrimeProps) {
        super(props);
        this.state = {
            algorithm: "atkin",
            number: "0",
        };
    }

    handleUserInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setStateFromInput(
            event.currentTarget.name,
            event.currentTarget.value,
        );
    };

    setStateFromInput(key: string, value: string) {
        this.setState({ [key]: value } as Pick<
            IIsPrimeState,
            keyof IIsPrimeState
        >);
    }

    calculatePrime = async (event: React.FormEvent) => {
        event.preventDefault();
        const url = `/api/is_prime?algorithm=${this.state.algorithm}&number=${this.state.number}`;
        const res = await fetch(url);
        const result: IIsPrimeResponse = await res.json();
        this.setState({ result: result.message.toString() });
    };

    render() {
        return (
            <form className={styles.form} onSubmit={this.calculatePrime}>
                <Markdown>{this.props.content}</Markdown>
                <div>
                    <label>
                        <input
                            name="algorithm"
                            type="radio"
                            value="atkin"
                            checked={this.state.algorithm === "atkin"}
                            onChange={this.handleUserInput}
                        />
                        Sieve of Atkin
                    </label>
                    <label>
                        <input
                            name="algorithm"
                            type="radio"
                            value="eratosthenes"
                            checked={this.state.algorithm === "eratosthenes"}
                            onChange={this.handleUserInput}
                        />
                        Sieve of Eratosthenes
                    </label>
                </div>
                <div>
                    <label>Number</label>
                    <input
                        name="number"
                        type="number"
                        step="1"
                        value={this.state.number}
                        onChange={this.handleUserInput}
                    />
                    <span>{this.state.result}</span>
                </div>
                <div>
                    <input type="submit" value="Submit" />
                </div>
            </form>
        );
    }
}

export default IsPrimeForm;
