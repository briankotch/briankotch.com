# Python-api

Live reloading, Dockerized python API

Designed to work natively or within docker-compose for easy development and deployable through Kubernetes as a deployment, service and ingress.

## Installing on a Mac

Virtual environment
```
brew install pyenv
pyenv install 3.12.4
```

Add to .zshrc (shell)
```
eval "$(pyenv init --path)"
```

## Environment Variables

* FLASK_RUN_HOST={binding address}
* FLASK_RUN_PORT={internal port for the api}
* OWM_API_KEY={api key}

Variables are managed through:

A [.env](/.env) file

Environment variables in the [docker-compose file](/docker-compose.yml)

Or set in the containers section of the [kubernetes yaml](/k8s.yaml)

## Run in docker-compose 

For easy developing, run the [docker-compose file](/docker-compose.yml)

```
docker-compose up
```

## Publish to Docker Hub 

Run ```publish.sh``` which updates [docker hub](https://hub.docker.com/repository/docker/briankotch/python-api) with the latest image under [briankotch/python-api:latest](https://hub.docker.com/repository/docker/briankotch/python-api). *need access to push to Docker Hub*

Deploy and expose

If you're using minikube, start and enable ingress:

```
minikube start
minikube addons enable ingress
```

Then run it in kubernetes

```
kubectl apply -f k8s.yaml
kubectl expose deployment python-api --type=NodePort --port=9999
```

Bonus round: ingress setup if you add minikube to expose the ip

```
minikube ip
```
And add to your /etc/host files
{ip address } hello-world.info

or call the ip address with -H python-api.info

## Python api running with Flask

Man, I forgot how much I loved Flask and Bottle. I have been fighting
with bloated libraries like Java and Powershell for so long 
that the ultralight weight implementation of a REST architecture really
revitalized my love for elegant, simple code. 

### Dynamically configured with Environment Variables

Loads the flask run time values with Environment Variables

Dotenv enable to allow easy and persistent overrides per environment in repos or CI/CD data 

I just wanted to show how I consider platform design. If it can't be controlled at the top 
and respond to different scenarios, it is a brittle design. If it requires three GUIs
with proprietary encryption schemes to set a few values, well, it's enterprise garbage
probably written by Adobe or Microsoft.

### Versioned API through blueprints

Modular rest routes with a basic api versioning scheme

Loads the version info and other text blobs into memory at boot rather than reading from the json on every request

Versioned routes will document themselves by returning their routing rules and available methods. It's like
swagger through basic reflection.

Weather calls out to the [OpenWeatherMap APi](http://openweathermap.org)

### Prime Number Calculators

I've written enough algorithms to solve this problem the way I have always done. Rely on the shoulders
of mathematicians and better programmers. That is to say, I ripped them off wholesale from the internet.

The important part is that they are both validating and constrained to reasonable use cases. 

In a full MVC framework, these would be modules that register themselves and versioning would
be controlled through a service layer so we could swap out algorithms that implement the same contract.

### Weather API

The weather api calls out to openweathermap.org with an API key stored in environment variables.

There's an easter egg in the V1 api.

## Things I would do if you were paying me

Unit tests and integration tests. 

A fully separated MVC architecture.

Secrets managed through k8s secrets.

Integration testing with mocks and stubs.

