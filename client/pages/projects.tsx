import React from "react";
import Header from "./components/Header";
import Divider from "./components/Divider";
import Footer from "./components/Footer";
import Nav from "./components/Nav";
import Propaganda from "./components/Propaganda";
import IsPrimeForm from "./components/projects/IsPrime";
import TicTacKoanForm from "./components/projects/TicTacKoan";
import styles from "./projects.module.scss";
import { isPromise } from "util/types";

interface IProjects {
    contentIsPrime: string;
}
interface IProjectsState {
    currentProject: ProjectType;
}

enum ProjectType {
    "IsPrime" = "IsPrime",
    "TicTacKoan" = "TicTacKoan",
}

class Projects extends React.Component<IProjects, IProjectsState> {
    constructor(props: IProjects) {
        super(props);
        this.state = {
            currentProject: ProjectType.IsPrime,
        };
    }

    static async getInitialProps() {
        const contentIsPrime = await require("./content/is-prime.md");
        console.log("getInitialProps", contentIsPrime);
        return {
            contentIsPrime: contentIsPrime,
        };
    }

    handleProjectChange = (event: React.FormEvent<HTMLSelectElement>) => {
        this.setState({
            currentProject:
                ProjectType[
                    event.currentTarget.value as keyof typeof ProjectType
                ],
        });
    };

    getCurrentProject = () => {
        switch (this.state.currentProject) {
            case ProjectType.IsPrime:
                return (
                    <section>
                        <h2 id="#is-prime">Is Prime</h2>
                        <IsPrimeForm content={this.props.contentIsPrime} />
                    </section>
                );
            case ProjectType.TicTacKoan:
                return (
                    <section>
                        <h2 id="#tic-tac-koan">Tic Tac Koan</h2>
                        <TicTacKoanForm boardX={3} boardY={3} />
                    </section>
                );
            default:
                return null;
        }
    };

    render() {
        const currentProjectSection = this.getCurrentProject();
        return (
            <div>
                <Header />
                <div className="main">
                    <div className="container">
                        <main className="content">
                            <section className="ink-strokes">
                                <h1>Projects</h1>
                                <p>
                                    My interactive portfolio of projects and
                                    practicals.
                                </p>
                                <select onChange={this.handleProjectChange}>
                                    <option value={ProjectType.IsPrime}>
                                        Is Prime
                                    </option>
                                    <option value={ProjectType.TicTacKoan}>
                                        Tic Tac Koan
                                    </option>
                                </select>
                            </section>
                            <Divider />
                            {currentProjectSection};
                        </main>
                        <Nav />
                        <Propaganda />
                    </div>
                    <Footer />
                </div>
            </div>
        );
    }
}

export default Projects;
