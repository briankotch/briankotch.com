import React, { ReactElement } from "react";
import styles from "./TicTacKoan.module.scss";

interface ITTKProps {
    boardX: number;
    boardY: number;
}

interface ITTKState {
    board: number[][];
    currentPlayer: Player;
}

enum Player {
    "X",
    "O",
}

class TicTacKoan extends React.Component<ITTKProps, ITTKState> {
    constructor(props: ITTKProps) {
        super(props);
        const board: number[][] = new Array(this.props.boardY);

        //initalize the array for the prop size
        //0: unset
        //1: X
        //2: O
        for (let y = 0; y < this.props.boardY; y++) {
            board[y] = new Array(this.props.boardX);
        }
        this.state = {
            board: board,
            currentPlayer: Player.X,
        };
        console.log(board);
    }

    static async getInitialProps() {
        return {
            boardX: 3,
            boardY: 3,
        };
    }

    handleCellClick = (event: React.FormEvent<HTMLDivElement>) => {
        const x = event.currentTarget.getAttribute("data-x");
        const y = event.currentTarget.getAttribute("data-y");
        const board = this.state.board;
        console.log(`Clicked x: ${x} y: ${y}`);
    };

    getBoard = () => {
        const rows: React.ReactElement[] = [];
        for (let y = 0; y < this.props.boardY; y++) {
            const columns: React.ReactElement[] = [];
            for (let x = 0; x < this.props.boardX; x++) {
                const cell: ReactElement = React.createElement(
                    "div",
                    {
                        id: `cell${x}:${y}`,
                        key: `cell${x}:${y}`,
                        "data-x": x,
                        "data-y": y,
                        className: styles.column,
                        onClick: this.handleCellClick,
                    },
                    `Cell x: ${x} y: ${y}`,
                );
                columns.push(cell);
            }
            rows.push(
                React.createElement(
                    "div",
                    { id: `row${y}`, key: `row${y}`, className: styles.row },
                    columns,
                ),
            );
        }
        return <div>{rows}</div>;
    };

    render() {
        return <div>{this.getBoard()}</div>;
    }
}

export default TicTacKoan;
