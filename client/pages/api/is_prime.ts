// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";

const URL_IS_PRIME = `http://python-api:${process.env.FLASK_RUN_PORT}`;
const QUERY_ALGORITHM = "algorithm";
const QUERY_NUMBER = "number";

export interface IIsPrimeResponse {
    code: number;
    message: string;
    number: number;
    success: boolean;
}
const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const algorithmVersion =
        req.query[QUERY_ALGORITHM] === "eratosthenes" ? "v1" : "v2";
    const number = req.query[QUERY_NUMBER];
    const requestURL = `${URL_IS_PRIME}/${algorithmVersion}/is_prime/${number}`;
    const isPrimeRequest = await fetch(requestURL, {
        method: "GET",
    });
    const data = await isPrimeRequest.json();
    return res.status(data.code).json(data);
};

export default handler;
