import React, { useRef } from "react";
import HCaptcha from "@hcaptcha/react-hcaptcha";
import styles from "./ContactForm.module.scss";

const HCAPTCHA_SITE_KEY: string = process.env
    .NEXT_PUBLIC_HCAPTCHA_SITE_KEY as string;

interface FormElements extends HTMLFormControlsCollection {
    name: HTMLInputElement;
    to: HTMLInputElement;
    message: HTMLTextAreaElement;
}

interface ContactFormElements extends HTMLFormElement {
    readonly elements: FormElements;
}

interface IContactFormProps {}

interface IContactFormState {
    token?: string;
    to?: string;
    name?: string;
    subject?: string;
    body?: string;
}

class ContactForm extends React.Component<
    IContactFormProps,
    IContactFormState
> {
    private captchaRef = React.createRef<HCaptcha>();

    constructor(props: IContactFormProps) {
        super(props);
        this.state = { subject: "Contact from BrianKotch.com" };
    }

    handleUserInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        event.preventDefault();
        this.setStateFromInput(
            event.currentTarget.name,
            event.currentTarget.value,
        );
    };

    handleUserTextArea = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
        event.preventDefault();
        this.setStateFromInput(
            event.currentTarget.name,
            event.currentTarget.value,
        );
    };

    handleVerificationSuccess(token: string) {
        this.setState({ token: token });
    }

    isValid() {
        return (
            this.isValidEmail(this.state.to) &&
            this.state.subject &&
            this.state.name &&
            this.state.body &&
            this.state.token
        );
    }

    isValidEmail(email?: string) {
        email = email ?? "";
        const regexp: RegExp = new RegExp(
            /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
        );
        return regexp.test(email);
    }

    sendEmail = async (event: React.FormEvent<ContactFormElements>) => {
        event.preventDefault();
        const body = JSON.stringify(this.state);
        const options = {
            body: body,
            headers: {
                "Content-Type": "application/json",
            },
            method: "POST",
        };
        const res = await fetch("/api/email", options);
        this.captchaRef.current?.resetCaptcha();
        this.setState({ token: "" });
    };

    setStateFromInput(key: string, value: string) {
        this.setState({ [key]: value } as Pick<
            IContactFormState,
            keyof IContactFormState
        >);
    }

    render() {
        return (
            <form
                className={styles.contactform}
                onSubmit={this.sendEmail}
                autoComplete="off"
            >
                <div>
                    <label>Your name:</label>
                </div>
                <div>
                    <input
                        name="name"
                        type="text"
                        placeholder="Your name"
                        required
                        className="form-input"
                        onChange={this.handleUserInput}
                    />
                </div>
                <div>
                    <label>Your email:</label>
                </div>
                <div>
                    <input
                        name="to"
                        type="email"
                        placeholder="Your email"
                        required
                        className="form-input"
                        onChange={this.handleUserInput}
                    />
                </div>
                <div>
                    <label>Subject:</label>
                </div>
                <div>
                    <input
                        name="subject"
                        type="text"
                        placeholder="Contact from BrianKotch.com"
                        required
                        className="form-input"
                        value={this.state.subject}
                        onChange={this.handleUserInput}
                    />
                </div>
                <div>
                    <label>Say something nice:</label>
                </div>
                <div>
                    <textarea
                        name="body"
                        placeholder="If you're a recruiter, be sure to read my resume."
                        onChange={this.handleUserTextArea}
                    />
                </div>
                <div>
                    <label>
                        Worse thing to happen to the internet since spam.
                    </label>
                </div>
                <div>
                    <HCaptcha
                        ref={this.captchaRef}
                        sitekey={HCAPTCHA_SITE_KEY}
                        onVerify={(token) =>
                            this.handleVerificationSuccess(token)
                        }
                    />
                    <input
                        type="submit"
                        value="Submit"
                        disabled={!this.isValid()}
                    />
                </div>
            </form>
        );
    }
}

export default ContactForm;
