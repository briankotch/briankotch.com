import React from "react";
import styles from "./Divider.module.scss";

class Divider extends React.Component {
    render() {
        return (
            <div className="container">
                <div className={`${styles.sakura_big}`} />
                <div className={`${styles.sakura_banner}`} />
            </div>
        );
    }
}

export default Divider;
