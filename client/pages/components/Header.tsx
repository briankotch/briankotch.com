import React from "react";
import styles from "./Header.module.scss";

interface IHeaderProps {
    videoEnded?: Boolean;
}

class Header extends React.Component<IHeaderProps, IHeaderProps> {
    constructor(props: IHeaderProps) {
        super(props);
    }

    renderVideo() {
        return (
            <video
                muted
                autoPlay={true}
                className={`${styles.hero} ${styles.poster}`}
                src="/media/yojimbo.mp4"
                poster="/images/yojimbo-start.jpg"
            />
        );
    }

    render() {
        return (
            <div className={styles.header}>
                <div className={styles.container}>
                    <div className={styles.panel} />
                    <div className={styles.content}>
                        <h1 className={styles.title}>Brian Kotch</h1>
                        {this.renderVideo()}
                    </div>
                    <div className={styles.panel} />
                </div>
            </div>
        );
    }
}

export default Header;
