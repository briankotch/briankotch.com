Resolving Json Modules for Fun and Profit

So, you want to import some JSON from a module using Typescript 
and you want the structure to determine your contracts.

At first blush, Typescript makes it very easy to import a JSON structure 
with some compilerOptions

```tsconfig.json
    ...
    "compilerOptions": {  
      "moduleResolution": "node",
      "resolveJsonModule": true,
      "esModuleInterop": true, 
      ...
    }
    ...
```

This then allows you to import a json file directly and get it back as an object with types all the way down.

```
import ProfileData from "../data/profile.json";
```

It works great with autocomplete and intellisense down to nested arrays and objects.

It is, however, readonly. None of the properties inferred from resolveJsonModule have getters, can be changed directly nor can the object be extended. 

You can, however, cast the object to an interface and get the best of both worlds.

