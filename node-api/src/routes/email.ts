import * as express from "express";
import { MailDataRequired } from "@sendgrid/helpers/classes/mail";
import { Request, Response, Router } from "express";
import {
    body,
    validationResult,
    Result,
    ValidationError,
} from "express-validator";
import emailController from "../controllers/email";
import Logger from "bunyan";

const EmailRouter: Router = express.Router();
const log: Logger = new Logger({ name: "node-api:emailRouter" });
const contactEmail: string = process.env.CONTACT_EMAIL as string;

// Validator chain middlware for incoming emails
const emailValidator = [
    body("to").isEmail(),
    body("subject").isString(),
    body("body").isString(),
    (req: Request, res: Response, next: Function) => {
        const error: Result<ValidationError> = validationResult(req);
        if (error.isEmpty()) {
            return next();
        }
        res.status(400).json({ errors: error.array });
    },
];

EmailRouter.post("/v1/email", emailValidator, (req: Request, res: Response) => {
    log.info("Sending email");
    const email: MailDataRequired = {
        to: req.body.to,
        from: contactEmail,
        subject: req.body.subject,
        text: req.body.body,
    };
    emailController.send(email);
    return res.send({ success: true });
});

export default EmailRouter;
