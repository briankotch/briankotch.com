import React from "react";
import Link from "next/link";
import styles from "./Nav.module.scss";

class Nav extends React.Component {
    render() {
        return (
            <nav className={styles.nav}>
                <Link
                    href="/"
                    replace={true}
                    data-hover="自宅"
                    data-active="Home"
                    className="button"
                >
                    <span>Home</span>
                </Link>
                <Link
                    href="/projects"
                    replace={true}
                    data-hover="プロジェクト"
                    data-active="Projects"
                    className="button"
                >
                    <span>Projects</span>
                </Link>
                <Link
                    href="/cv"
                    replace={true}
                    data-hover="履歴書"
                    data-active="CV / Resumé"
                    className="button"
                >
                    <span>CV / Resumé</span>
                </Link>
                <Link
                    href="/contact"
                    replace={true}
                    data-hover="私に連絡して"
                    data-active="Contact"
                    className="button"
                >
                    <span>Contact</span>
                </Link>
            </nav>
        );
    }
}

export default Nav;
