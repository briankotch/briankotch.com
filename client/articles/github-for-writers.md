Ever lose your writing? It's one of the most heart breaking things an author can endure next to the learned appreciation of constant rejection. Even in the age of dropbox, google docs, and zillions of other synced services, one word document saved into a weird place and some hardward catastrophic failures can lead to soul crushing sting of brilliance lost. Fortunately, we as writers can utilize the very same free technologies programmers use to preserve every thought, track every change and keep our thoughts safe.

In this series of articles, I am going to walk you through setting up a free workflow that will not only allow you to track every edit, bullet proof your workflow and show you how youc an have a showcase for your work without any cost or submission fees beyond registering your domain names.

Our medium just so happenes to share the same language as every major programming language. Code, applications, poems, short stories or novels they are all just words in a text file. As writers, we do not have the same resource demands as programmers. While they strive for terseness in their language, we are free to bloat our chapters with only a few more kilobytes. Without binaries, images, databases, memstores and the other complex infrastucture concerns, we can focus our efforts on keeping our words safe, making it easy to track changes and publish our words without worrying about markup, incompatible standards or heavy CMSes and expensive content sites.

In this series, you will create a showcase for your words in there native format.

We'll be building a writer-oriented workflow utilizing:
Markdown
Hugo
Github pages

The end goal will be:

Your works remain unchanged minus a little bit of front matter meta data.
A beautiful website built using free technology with static results free of any infrastructure headaches.
A free host to which you can map your own domain for branding, SEO optimization and social engagement.

After all, we live in a world oriented for technologies that render imagination irrelevant. When our graphics engine is a strong description in the active voice, we can build worlds using the freemium models designed to coax startups into squandering their seed capital.

Right now, you have a choice. You can buy template-based content providers that overcharge for basic technology or you can learn a few industry best practices to establish your web presence without any more investment than buying a domain name.

It will be broken into sections.

1. Creating a code repository for your writings.
2. Creating an entire website with only a few commands and storing your writing in a native and open standard immune to vendor lockin and proprietary format.
3. Publish your writings while doing more than commiting and pushing your important change.