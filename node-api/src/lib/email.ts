import * as nodemailer from "nodemailer";
import dotenv from "dotenv";
import Mail from "nodemailer/lib/mailer";
import SMTPTransport from "nodemailer/lib/smtp-transport";

dotenv.config();

class SmtpOptions implements SMTPTransport.Options {
    host = process.env.SMTP_ADDRESS;
    port = parseInt(process.env.SMTP_PORT as string);
    user = process.env.SMTP_USERNAME;
    auth = {
        user: process.env.SMTP_USERNAME,
        pass: process.env.SMTP_PASSWORD,
    };
}

export class MailOptions implements Mail.Options {
    from: string;
    to: string;
    subject: string;
    html: string;

    constructor(from: string, to: string, subject: string, body: string) {
        this.from = from;
        this.to = to;
        this.subject = subject;
        this.html = body;
    }
}

export class EmailSender {
    private _transporter: nodemailer.Transporter;
    private _smtpConfig: SmtpOptions;

    constructor() {
        this._smtpConfig = new SmtpOptions();
        this._transporter = nodemailer.createTransport(this._smtpConfig);
    }

    async sendMail(options: MailOptions) {
        this._transporter.sendMail(options, (error, info) => {
            if (error) {
                Promise.reject(error);
            } else {
                Promise.resolve(`Message sent $info.response`);
            }
        });
    }
}
