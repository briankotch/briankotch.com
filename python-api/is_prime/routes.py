from flask import abort, Blueprint
from flask import current_app as app
from werkzeug.exceptions import BadRequest
from is_prime import atkins, eratosthenes
import functools, json, logging

v1_bp = Blueprint(
    'v1', __name__,
    template_folder='templates',
    static_folder='static'
)

v2_bp = Blueprint(
    'v2', __name__,
    template_folder='templates',
    static_folder='static'
)

# decorator function for validating numbers
# expects a number int, gets cute with edge cases
def validate_number(func):
    @functools.wraps(func)
    def wrapper_validate_number(*args, **kwargs):
        logging.info('Validating')
        number = kwargs['number']
        try:
            number = int(number)
        except ValueError:
            raise BadRequest("Must be an integer")
        if number <= 0:
            raise BadRequest("Must be a positive integer, unless you want to get philosophical.")
        if number >= 1000000:
            raise BadRequest("Number is too large. Edge case test detected. Try v1 as v2 creates arrays.")

        kwargs['number'] = number
        return func(*args, **kwargs)
    return wrapper_validate_number



# Just using dumb logic validation here in decorators
# Inelegant, but reusable and easy to read
# I'd either use typed urls and Marshmallow objects to validate real requests
@v1_bp.route("/is_prime/<number>")
@validate_number
def is_prime(number):
    return format_response(number, eratosthenes.is_prime(number))

@v2_bp.route("/is_prime/<number>")
@validate_number
def is_prime(number):
    return format_response(number, atkins.is_prime(number))

def format_response(number, is_prime):
    response = {
        "code": 200,
        "number": number,
        "message": is_prime,
        "success": True,
    }
    return json.dumps(response)
    