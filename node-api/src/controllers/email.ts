import mailer from "@sendgrid/mail";

mailer.setApiKey(process.env.SENDGRID_API_KEY as string);

class EmailController {
    send(email: mailer.MailDataRequired) {
        mailer.send(email);
    }
}

const emailController = new EmailController();

export default emailController;
