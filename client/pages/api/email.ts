import type { NextApiRequest, NextApiResponse } from "next";

const URL_EMAIL = `http://node-api:${process.env.EXPRESS_PORT}/v1/email`;

interface IEmailApiResponse {
    success: boolean;
}

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
    const payload = {
        to: req.body["email"],
        subject: req.body["subject"],
        body: req.body["body"],
    };
    const formValues = new URLSearchParams(req.body);
    const emailRequest = await fetch(URL_EMAIL, {
        method: "POST",
        headers: {
            "content-type": "application/x-www-form-urlencoded",
        },
        body: formValues,
    });
    const data: IEmailApiResponse = await emailRequest.json();
    return res.status(200).json(data);
};

export default handler;
