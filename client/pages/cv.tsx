import React from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Nav from "./components/Nav";
import Propaganda from "./components/Propaganda";
import Divider from "./components/Divider";
import Markdown from "react-markdown";
import styles from "./cv.module.scss";

interface ICVProps {
    content: string;
}
class CV extends React.Component<ICVProps> {
    static async getInitialProps() {
        const content = await require("../resume.md");
        return {
            content: content,
        };
    }

    render() {
        return (
            <div>
                <Header />
                <div className="main">
                    <div className="container">
                        <main className="content">
                            <section className="ink-strokes">
                                <h1>My Resume / CV</h1>
                                <div className="centerContainer">
                                    <div>
                                        <a
                                            data-hover="ダウンロード"
                                            data-active="Download"
                                            href="../resume.pdf"
                                            className="button"
                                        >
                                            <span>Download PDF</span>
                                        </a>
                                    </div>
                                </div>
                            </section>
                            <Divider />
                            <section>
                                <Markdown>{this.props.content}</Markdown>
                            </section>
                        </main>
                        <Nav />
                        <Propaganda />
                    </div>
                    <Footer />
                </div>
            </div>
        );
    }
}

export default CV;
