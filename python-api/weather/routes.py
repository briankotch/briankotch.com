from flask import abort, Blueprint, render_template
from flask import current_app as app
from weather import openweathermap, theendtimes
import functools, json, re

v1_bp = Blueprint(
    'v1', __name__,
    template_folder='templates',
    static_folder='static'
)

v2_bp = Blueprint(
    'v2', __name__,
    template_folder='templates',
    static_folder='static'
)

# decorator function for validating zipcodes
def validate_zip_code(func):
    @functools.wraps(func)
    def wrapper_validate_zip_code(*args, **kwargs):
        zip_code = kwargs['zip_code']
        if not re.match("[0-9]{5}", zip_code):
            abort(400, "Not a 5 digit zip code")
        kwargs['zip_code'] = zip_code
        return func(*args, **kwargs)
    return wrapper_validate_zip_code

@v1_bp.route("/weather/<zip_code>")
@validate_zip_code
def get_weather(zip_code):
    text = theendtimes.get_weather(zip_code)
    return render_template('base.html', site="python api", title=zip_code, text=text)

@v2_bp.route("/weather/<zip_code>")
@validate_zip_code
def get_weather(zip_code):
    return openweathermap.get_weather(zip_code)

