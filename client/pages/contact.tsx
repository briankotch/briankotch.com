import React from "react";
import ContactForm from "./components/ContactForm";
import Divider from "./components/Divider";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Nav from "./components/Nav";
import { LogoClassNames, Logo } from "./components/Logo";
import Propaganda from "./components/Propaganda";
import styles from "./contact.module.scss";

class Contact extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <div className="main">
                    <div className="container">
                        <main className={`content ${styles.plants}`}>
                            <section className={`ink-strokes`}>
                                <h1>Get in touch!</h1>
                                <div className={styles.logos}>
                                    <Logo
                                        href="https://www.linkedin.com/in/briankotch/"
                                        className={LogoClassNames.LinkedIn}
                                    />
                                    <Logo
                                        href="https://bitbucket.org/briankotch/"
                                        className={LogoClassNames.Bitbucket}
                                    />
                                </div>
                            </section>
                            <Divider />
                            <section>
                                <ContactForm />
                            </section>
                        </main>
                        <Nav />
                        <Propaganda />
                    </div>
                    <Footer />
                </div>
            </div>
        );
    }
}

export default Contact;
