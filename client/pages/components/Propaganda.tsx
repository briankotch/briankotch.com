import { randomInt } from "crypto";
import React from "react";
import util from "util";

import styles from "./Propaganda.module.scss";

class Propaganda extends React.Component {
    render() {
        const index = Math.floor(Math.random() * (4 - 1) + 1);
        const propaganda = util.format("propaganda%d", index);
        console.log(propaganda);
        return <aside className={`${styles.art} ${styles[propaganda]}`} />;
    }
}

export default Propaganda;
