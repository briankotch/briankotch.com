from flask import abort, Blueprint
from flask import current_app as app
import copy, json, re

versions = {}
with open('version/versions.json') as json_file:
    versions = json.load(json_file)
    

default_bp = Blueprint(
    'default', __name__,
    template_folder='templates',
    static_folder='static'
)

version_bp = Blueprint(
    'version', __name__,
    template_folder='templates',
    static_folder='static'
)

@default_bp.route('/version')
@version_bp.route('/version/<string:version>')
def get_version(version = ""):
    versionsLookup = copy.copy(versions)
    version = version or versionsLookup["currentVersion"]
    versionsLookup["requestedVersion"] = version
    majorVersion = re.search('[0-9]', version).group(0)
    rules = {}
    for rule in app.url_map.iter_rules():
        app.logger.info(rule.rule)
        if re.match(f"/v{majorVersion}", rule.rule):
            rules[rule.rule] = ",".join(rule.methods)
    if len(rules) == 0:
        abort(400, 'Bad version number')
    versionsLookup["rules"] = rules
    return versionsLookup