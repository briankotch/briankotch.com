"use strict";
import { Application, Request, Response } from "express";
import dotenv from "dotenv";
import Express from "express";
import Logger from "bunyan";
import BioRouter from "./routes/bio";
import EmailRouter from "./routes/email";

dotenv.config();

const log: Logger = new Logger({ name: "node-api" });
const PORT: number = parseInt(process.env.EXPRESS_PORT as string);
const HOST: string = process.env.EXPRESS_ADDRESS as string;
const app: Application = Express();

app.get("/", (req: Request, res: Response) => {
    return res.send("briankotch.com - node-api");
});

// Middleware
app.use(Express.json());
app.use(Express.urlencoded({ extended: true }));

// Routes
app.use(BioRouter);
app.use(EmailRouter);

log.info(`Listening on ${PORT} ${HOST}`);

app.listen(PORT, HOST);
