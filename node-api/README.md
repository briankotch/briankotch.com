# Node-api

Provides backend functionality written in typescript / node.

Unit tests written in Jest with mocks.

Basic implementation of an MVC framework, classes, modules

Linted and made pretty with eslint and prettier.

Secrets managed in environment variables.

## Typescript

Built against ES2020 and resolveJSONModules

```
      "module": "commonjs",
      "moduleResolution": "Node",
      "resolveJsonModule": true,
      "esModuleInterop": true,
```

Allows typing of imported json data stores and building to Node 14.

## Environment Variables 

Designed to be loaded with secrets and orchestration but can be controlled for development with a dotenv file.

```
EXPRESS_ADDRESS="0.0.0.0"
EXPRESS_PORT=8080
CONTACT_EMAIL={secret}
CONTACT_NUMBER={secret}
SENDGRID_API_KEY={secret}
```

## Building and Running

Yarn commands are defined in the package.json

```yarn start``` build and compile a production version
```yarn dev``` tscompile with nodemon for live reloading
```yarn test``` run jest tests
```yarn lint``` dry-run to find errors
```yarn lint:fix``` find and fix errors