# Client

## NextJs + TypeScript + Prettier + Microservice Architecture

The dev tool chain is the thing I care about the most. Automation is my safety net. Types and lint keep things clean and reduce bugs.

## Important sections

### ContactForm.tsx 

It does a lot just to keep my email safe from spam bots and over-eager recruiters. I am an object-oriented guy by habit, so I go with the class paradigm because it makes sense to me. I like types, interfaces and classes more than I like anonymous functions and hooks. 

It uses state, generic event handlers and typescript magic to populate the contact form with the bare minimum of properties. It requires HCaptcha and sends the token to the NextJS api. That api then talks to the internal node-api service that has my SendGrid secrets and finally sends me an email

### CV.tsx

Updating my resume is a chore. To make it easier, I just have my resume in a markdown file. That markdown file gets loaded by webpack 5's asset source and gets rendered by React Markdown. I also have a git prehook that updates the downloable PDF on commit.


# Installation 

```
yarn install
yarn dev
```

## Testing

```
```