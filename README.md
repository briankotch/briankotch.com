# BrianKotch.com

Welcome to my completely over-engineered website.
I use it to keep my skills sharp and to demonstrate my skills as a full stack developer and system architect.

* client 
* node-api
* python-api
* Devops / CICD

## client

*React/NextJS front end.*

* Typescript
* Linted
* Cypress tests
* Sass-y front end

## node-api

*Express node api.*

## python-api

*Flask python api.*

## Devops / CICD

*Docker and Bitbucket Pipelines.*

## Install Docker on Ubuntu

https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository

## Initial Server Setup

https://www.linuxandubuntu.com/home/host-multiple-websites-on-docker-containers

Store ssl certificates in /etc/ssl_certs

### Create docker network

```
docker network create nginx-proxy
```




## Running through systemctl

Ubuntu centric. 

```
cp docker-compose.service /etc/systemd/system
```

Create a .conf file containing key=value pairs for the environment variables



Update /etc/systemd/system/docker-compose.service

Change user, group, working dir and paths to both docker and the docker compose

Make sure the EnvironmentFile path points to the .conf file

Reload the configuration.

```
sudo systemctl daemon-reload
sudo systemctl start docker-compose.service
sudo systemctl status docker-compose.service
```