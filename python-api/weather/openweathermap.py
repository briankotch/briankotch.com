from dotenv import load_dotenv
import logging, os, requests

load_dotenv(override=False)

api_key = os.getenv("OWM_API_KEY")
owm_url =  "https://api.openweathermap.org/data/2.5/weather?zip={zip_code}&appid={api_key}"

def get_weather(zip_code):
    try:
        response = requests.get(owm_url.format(zip_code=zip_code, api_key=api_key))
    except Exception as e:
        response = { "error": e }
    return response.json()