import React from "react";
import styles from "./Footer.module.scss";

class Footer extends React.Component {
    render() {
        return (
            <div className={styles.footer}>
                <div className={styles.container} />
            </div>
        );
    }
}

export default Footer;
