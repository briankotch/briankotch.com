import React from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Nav from "./components/Nav";
import Propaganda from "./components/Propaganda";
import styles from "./bio.module.scss";

class Bio extends React.Component {
    render() {
        return (
            <div>
                <Header />
                <div className="main">
                    <div className="container">
                        <main className="content">
                            <h1>Who am I?</h1>
                        </main>
                        <Nav />
                        <Propaganda />
                    </div>
                    <Footer />
                </div>
            </div>
        );
    }
}

export default Bio;
