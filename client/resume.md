
# Brian Kotch - Full Stack Developer / Team Lead

-----------------------
LinkedIn: [https://www.linkedin.com/in/briankotch]

## Who I am

A platform architect who loves making microservices scalable and testable and who thrives in the med tech space backed by opensource technologies.

A lover of free and powerful languages like Python. NodeJS, Javascript made sane through Pydantic, Typescript, golang and Java.

A problem solver who has been a dev manager, a devops lead and a principal full stack developer. I wear many hats equally well and I am most comfortable building teams and solving problems. I am obsessed with architecture and automation.

If I can't make a server deploy to Kubernetes and reconfigure itself on boot, I will find a way. Ideally, I could teach junior folks the skills needed to create beautiful, flexible platforms as code.

Recruiters: Email me first to schedule a time to talk. All unsolicited phone calls will be ignored since you probably didn’t read my resumé / CV very well.  

## What I have done

-----------------------

### Synchrony - System Architect and Vice President of API Architecture

#### *February 2023 - Current*

Key Technologies: **Mulesoft, AWS, Java, NodeJS, Typescript, MSSQL, Redis**

Scaled a monolithic ColdFusion application into a microservice architecture.

Lead multiple team on an enterprise rewrite as a system architect and drum beater.

Implemented a JWT-token based authorization system with central cache revocation.

Re-invented a Mulesoft architecture into a multi-layered microservice architecture to scale web requests, background jobs and database operations.

Cut off the monolith from the public internet by rewriting the front end in React / TypeScript / Vite.

Enabled a CI/CD workflow and SDLC best practices with a dockerized build environment, bitbucket build steps and integration tests written in Postman and run by Newman. 

Built a containerized stack for a microservice architecture using Docker and bookworm slim images.

### Elation Health - Senior Software Developer

#### *March 2022 - February 2023*

Key Technologies: **Django, Apache Kafka, Confluent, Docker, Terraform, Test Driven Development, Python**

Saved an overworked mysql database by building an event streaming model that pushed the latest and greatest versions of business objects to downstream consumers.

Worked on the external platform team to implement the event streaming in Kafka and Confluent, interfaced with every department to validate and prove out the organic data model. Worked with stakeholders to deliver event streaming to satisfy high-scale and critical business requirements.

Ensured atomicity and uptime by reading outbox changes (Debezium CDC) on a replica and dispatching messages to internal topics that then used serialized, multi-threaded processors to "hydrate" data and send it down to customer-facing consumers.

Build a multi-service architecture in Docker and Terraform to manage the heavy event stream stack (zookeeper, debezium, kafka). Development environments could spin up the stack through docker compose and downstream environments managed their configuration downstream

Built a performant hash-bashed event queue to aggregate many changes to related business models into one event record.

Wrote an extensive suite of unit and integration tests to verify the many different types of dynamic object models.

Managed a team of contractors to deliver documentation, unit tests and event streams for many business objects with Elation's Django ORM models.

### DesignMind - Principal Consultant

Key Technologies: **Azure, AWS, Java**

#### *May 2020 - March 2022*

Worked as DesignMind's resident AWS and Azure smoke jumper brought in to solve major platform problems.

Deployed Dotnet Core / Java applications to Azure resource groups using App Service and Azure Container Services. Setup containerized environments running CosmosDB and managed their secrets through KeyVault.

Lead the technical implementation as Devops and Sysadmin for a financial company's acquisition. The previous platform admins left a decaying mess of insecure AWS windows machines running unpatched Windows 2012 R2 machines disconnected, without central management, and I was called in to:

- Modernize their AWS infrastructure by implementing CodeDeploy, AWS Systems Manager and New Relic

- Roll out a suite of security software including ZScaler, Qualys and Crowdstrike

- Fix the myriad vulnerabilities discovered by the security software by rolling out centralized updates through Windows Active Director and AWS SSM.

- Reinvent precious production servers with brittle configurations to be custom AMI images with sysprep and user data to dynamically configure and launch replicas under load balancing.

### Minds - Full Stack Developer - Director of Development

#### *January 2019 - May 2020*

Key Technologies: **PHP, Angular, Docker, Elastic Search, Cassandra, Kubernetes**

**As a developer:**

Led development efforts for their open source social network.

Built out a KITE algorithm for tracking user state changes in elastic search.

Wrote a dynamic, ad-hoc permissions system that applied role based entities.

Released lots of features to a fast, high-availability social network. Lots of lessons learned about denormalization, writing of data to multiple places and a polished hatred for Cassandra.

Got in deep with ElasticSearch, writing performant queries and upgrading from 5 to 6 and beyond.

**As a director:**

Organized and optimized our gitlab work flows, ran scrum, managed the sprints.

Led a full-scale rewrite of both the web and mobile platforms by breaking down complex mocks into specifications.

Coordinated three teams of developers, QA and our stakeholders.

Brought in automated E2E testing, full CI flow for mobile deployments and ironed out many bumps in the review / QA process.

Got good at Kubernetes and automated deployments of their QA review sites and helped transform the pods into a much closer version of production.

Defended the developers from rather myopic senior management while demonstrating the effectiveness of more mature and rigorous development practices.

### Provata Health - Full Stack Developer (Current)

#### *July 2017 - December 2018*

Location: *Remote, Portland, Oregon*

Key Technologies: **React, React Native, Nodejs, Java (Jersey), AWS, Docker, AlienVault**

Led the implementation of their Alien Vault security platform. Investigated vendors, proposed the platform to management and then deployed the sensors to lock down their AWS platform and ensure HIPAA compliance.

Built a stateless scoring back end service that gathered heterogenous health information about a user and awarded them points on dynamic criteria. By streaming and paralleling multiple requests and using lambda functions to transform them into scores, Provata can calculate a user's vitals on the fly and under 20 milliseconds.

Helped people get healthy by integrating their vitals data into Provata's health platform.

Built out their RESTful microservices architecture using Jersey with light controllers and fat services that were built to be testable and idempotent. Fought many battles with Gradle because of it.

Developed features from mock to production along the entire stack. Created database schema in liquibase. Wrote backend code in Java. Served the api endpoints via nodejs and delivered responsive, frontend code to the web via React and React Native.

Implemented an in memory caching solution to relieve bottle necks. Kept it simple. Made it work faster by not hammering the database every time we needed to check account information.

### InvisionApp Inc - Senior Full Stack Developer

#### *July 2015 - June 2017*

Location: *Remote*

Key Technologies: **Golang, Nodejs, React, Docker, MySQL**

Stopped worrying and learned to love Docker and docker-compose. Why yes, I will store my entire service configuration in a yaml file that auto builds and pulls in dependencies with virtual networking and environment variables. <3

Heated up the production CPUs a couple of times, but never caused a major outage deploying code to a very large user base. (once or twice, but I was on pager duty and lead the RCAs).

Realized pixel-perfect and snappy ui's for millions of users using Angular and React.

Built restful micro services in Node.js. Verbs, plural entities, versioned endpoints, CORS compatible.

Designed, architected and lead a project to centralize Invision's permissions.

Made constant improvements to the entire development stack.

Chipped away at the constant glacier of TDD and implementing full unit and integration testing.

Begrudgingly learned coldfusion to help support legacy apps.

Got really good at git, codeship, karma and chai. Went from git-diot to the dude you call when you need to rebase that long running feature branch.

Debugged complex client side applications from modifying headers and watching hundreds of requests in the network tab.

Developed a polished hatred for Firefox and Chrome and IE. Web browsers suck.

### Moody's Analytics - Senior Full Stack Developer - Contract

#### *June 2015 – August 2015*

Location: *San Francisco, California*

Key technologies: **C#, Angular, WebAPI, MongoDB, MSSQL**

Wrote a RESTful webservice to provide real time analysis of financial data.

Wrote an HTML5 uploader directive to receive and process large customer data files.

Built an angular app to consume RESTful webservice data and provide robust charting and analytics with interactive filters.

### Freelancer - Self Employed

#### *June 2014 - July 2015*

Key technologies: **Angular, Javascript, HTML5, CSS3, PHP**

Designed custom, responsive web apps in HTML5, CSS3, Javascript and PHP.

Worked with Diagnostic Partners to modernize their patient study management web app.

Maintained Martin De Porres web site.

### Littler - Senior Web Developer - Contract

#### *January 2015 - June 2015*

Location: *San Francisco, California*

Key technologies: **C#, Angular, WebAPI, MongoDB, MSSQL, Grunt, Karma, Jasmine**

Wrote a RESTful webservice to provide real time analysis of financial data.

Wrote an HTML5 uploader directive to receive and process large customer data files.

Built an angular app to consume RESTful webservice data and provide robust charting and analytics with interactive filters.

### Fluid - Implementation Specialist

#### *March 2013 - June 2014*

Location: *San Francisco, California*

Key Technologies: **HTML5, CSS3, Node.js**

Worked as an Implementation engineer. I implemented the e-commerce experiences for Reebok and Oakley.

Leveraged Amazon Web Services, Node.js and CSS3 to deliver customizable shopping experiences. I developed automation services to help migrate and update large amounts of customer data.

Worked closely with clients to transform their inventory data and pricing information in global, localized interactive 3d models tightly integrated into their site.

Strove to improve the internal software, best practices and maintainability of a complex, multi-tiered software as a service publishing platform.

Gathered user requirements, lead technical teams and helped launch two major re-platformings across the globe.

### MyLikes - Full Stack Developer

#### *January 2012 - January 2013*

Location: *San Francisco, California*

Key technologies: **Angular, HTML5, CSS3, Java, Android, Streaming Video**

Rebooted my tech career from enterprise development to the world of start-ups and open source.

Failed miserably in this role, but I learned a lot.

Developed a high-availability video sharing platform using Python and Javascript with JQuery and CSS3.

Created a mobile version for Android.

### Ricoh - Business Solutions Specialist

#### *October 2009 - January 2012*

Location: *Auckland, New Zealand*

Key Technologies: **C#, Document Management**

Managed implementation projects by meeting with the customers, gather- ing their requirements and translating their needs into fully documented project plans.

Implemented enterprise solutions by deploying services, databases and clients with custom work-flows written in C#, CSS and OS scripting.

Linked clients’ line-of-business systems to their new document management architecture by creating custom connectors, staging databases and modifying HTML/CSS and GUI forms.

Provided direct support for all levels of clients from user training to change requests.

Improved the productivity of small to medium enterprises by delivering custom solutions designed to work within their budgets and existing infrastructure.

### CSSI - Developer

#### *October 2007 - June 2007*

Location: *Lewisburg, Pennsylvania*

Key Technologies: **C#, Document Management**

Developed a document management system in C# and ASP.net.

Generated custom plugins receiving documents and extracted information from high-volume scanning (OCR) and plugins releasing documents to archival and document management systems (Sharepoint, Laserfiche, Documentum).

Created custom approval and exception workflows to move information based on information extracted from the documents.

### CRS - Developer

#### *August 2005 - June 2007*

Location: *Williamsport, Pennsylvania*

Key Technologies: **C++, C#, ASP.net**  

Programmed interactive voice response systems in C++.

Redesigned the app to be a SaaS model in C# with a C++ engine on the back end.

### US Army - E4 Specialist - 63B

#### *April 1998 - April 2004*

Location: *Hazleton, Pennsylvania*

Key Technologies: **Wrenches, Grease, Rifles**

Worked as a 63B (Light Wheel Diesel Mechanic).

Received an Army Medal of Accommodation for rescuing a General from a broken-down HUMVV.

## Education

### Bloomsburg University

#### 2001 - 2005

Location: *Bloomsburg, Pennsylvania*

Bachelor of Arts, English Language and Literature

Minor, Geography

# References  

-----------------------

Available upon request.
