from flask import current_app

# https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
def is_prime(number): 
    # Initialize an array with True
    # 1. Create a list of consecutive integers from 2 through n
    primes = [True for i in range(number+1)]
    # 2. Let p equal 2, the smallest prime number
    p = 2
    # 3. Enumerate the multiples of p
    while (p * p <= number):
        p += 1
        if (primes[p] == True):
            for i in range(p * p, number+1, p):
                primes[i] = False
    
    for p in range(2, number+1):
        if primes[p]:
            print(p)
    
    isPrime = primes[p]

    current_app.logger.info(f"{number} is prime: {isPrime}")

    return isPrime