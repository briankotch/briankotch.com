import React from "react";
import Link from "next/link";
import styles from "./Logo.module.scss";

export enum LogoClassNames {
    Bitbucket = "Bitbucket",
    LinkedIn = "LinkedIn",
    Mysql = "Mysql",
}

interface LogoProps {
    href: string;
    className: LogoClassNames;
}

export class Logo extends React.Component<LogoProps> {
    render() {
        return (
            <a
                href={this.props.href}
                className={`logo ${styles.logo}
                ${styles[this.props.className]}`}
            />
        );
    }
}

export default Logo;
