import ProfileData from "../data/profile.json";
import dotenv from "dotenv";

dotenv.config();

interface IProfile {
    contact_number: string;
    contact_email: string;
    first_name: string;
    last_name: string;
    jobs: IJob[];
}

interface IJob {
    name: string;
    position: string;
    start_date: string;
    end_date: string;
    accomplishments: string[];
    tags: string[];
}

/*
resolveJSONModule creates typed, but read-only objects
Casting the generic object to an interface allows us to expose the properties
*/
const bio = <IProfile>ProfileData;
bio.contact_email = process.env.CONTACT_EMAIL as string;

export { IProfile, IJob, bio };
