/** @type {import('next').NextConfig} */
module.exports = {
    output: 'standalone',
    reactStrictMode: true,
    webpack: function (config) {
        config.module.rules.push({
            test: /\.md$/,
            type: "asset/source",
        });
        return config;
    },
};