from flask import Flask, Blueprint, render_template, jsonify
from dotenv import load_dotenv
from werkzeug.exceptions import BadRequest
from is_prime import routes as primeRoutes
from weather import routes as weatherRoutes
from version import routes as versionRoutes
import json

load_dotenv(override=False)

with open('README.md') as readme_md:
    readme = readme_md.read().split('\n')

def create_app():
    app = Flask(__name__, instance_relative_config=True)
    
    # Load specific configs here
    with app.app_context():
        # TODO auto import blueprints
        app.register_blueprint(primeRoutes.v2_bp, url_prefix="/", name="prime_default")
        app.register_blueprint(primeRoutes.v2_bp, url_prefix="/v2", name="prime_v2")
        app.register_blueprint(primeRoutes.v1_bp, url_prefix="/v1", name="prime_v1")
        app.register_blueprint(weatherRoutes.v2_bp, url_prefix="/", name="weather_default")
        app.register_blueprint(weatherRoutes.v2_bp, url_prefix="/v2", name="weather_v2")
        app.register_blueprint(weatherRoutes.v1_bp, url_prefix="/v1", name="weather_v1")
        app.register_blueprint(versionRoutes.default_bp, url_prefix="/", name="version_default")
        app.register_blueprint(versionRoutes.version_bp, url_prefix="/", name="version_v1")


        @app.route("/")
        def index():
            # TODO render the markdown 
            return render_template("base.html", site="python api", title='python api', text=readme)
    
        @app.errorhandler(Exception)
        def bad_request(error):
            response = {        
                "code": error.code,
                "message": error.description,
                "success": False,
            }
            return json.dumps(response);
    return app    









